package com.example.myapplication;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextClock;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ArrayList<Block_Time> listBlock_Time;
    Block_Adapter _Block_TimeListViewAdapter;
    ListView listViewBlock_Time;
    //=====================================================
    TextClock clock_Day,clock_Time;
    Button button_ADD;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Intent intent_input = new Intent(MainActivity.this,InputData.class);
        //startActivity(intent_input);
        Log.e("debug","Bat dau he thong");

        String BigString = readData();
        Log.e("Main","BigString:" + BigString);
        if(BigString != null)
        {
            // string có dữ liệu
            listBlock_Time = new ArrayList<Block_Time>(BigString2List(BigString));
        }else{
            listBlock_Time = new ArrayList<Block_Time>();
        }
        button_ADD = (Button) findViewById(R.id.button);
        listViewBlock_Time = (ListView) findViewById(R.id.List_ID);

        clock_Day = (TextClock) findViewById(R.id.TextClock_Day) ;      // gắn id cho đối tượng
        clock_Day.setFormat24Hour("E,'Ngày' d 'tháng' M");      // cài đặt định dạng
        clock_Time = (TextClock) findViewById(R.id.TextClock_Time) ;    // tương tự trên
        clock_Time.setFormat24Hour("hh:mm a");

        _Block_TimeListViewAdapter = new Block_Adapter(listBlock_Time);// tạo Adaper mới
        listViewBlock_Time.setAdapter(_Block_TimeListViewAdapter);  // cài adaper vào list

        listViewBlock_Time.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Block_Time temp = (Block_Time) _Block_TimeListViewAdapter.getItem(i);   // số thứ tự item là i
                                                                                        // tương đương với lấy phần tử thứ i của aray
                Intent intent_Add_time = new Intent(MainActivity.this,InputData.class);
                intent_Add_time.putExtra("Block",temp.toString());
                Log.e("debug","Send to GET : " + temp.toString());

                intent_Add_time.putExtra("Command","GET");
                startActivity(intent_Add_time);
            }
        });
//=================================================================================
        Intent integer = getIntent();
        String command = " ";
        String StringBlock = " ";
        try {
            command = integer.getStringExtra("command");
            StringBlock = integer.getStringExtra("StringBlock");
        }catch (Exception e)
        {
            Log.e("ERROR","ERROR getStringExtra'command'");
        }
        //
// kiểm tra null
        Log.e("debug","COMMAND: " + command);
        Log.e("debug","StringBlock: " + StringBlock);
    if(StringBlock != null)
    {
        if(command.equals("SAVE"))
        {
            // fix lại chuyển thành get
            Block_Time temp_block = new Block_Time(StringBlock);
            if(temp_block.get_ID()>= listBlock_Time.size())
            {
                Log.e("debug","Go to Creat new Block");
                listBlock_Time.add(temp_block);
                _Block_TimeListViewAdapter.notifyDataSetChanged();
                SaveFile(listBlock_Time);
            }else{
                Log.e("debug","Go to Set Block : "+temp_block.get_ID());
                listBlock_Time.set(temp_block.get_ID(),temp_block);
                _Block_TimeListViewAdapter.notifyDataSetChanged();
                SaveFile(listBlock_Time);
            }


        }else if(command.equals("DEL"))
        {
            Log.e("debug","Goto Function DEL");
            //String Str_block = integer.getStringExtra("StringBlock");
            Log.e("debug","StringBlock Read : " + StringBlock);
            try{
                listBlock_Time.remove(Integer.parseInt(String.valueOf(Integer.parseInt(StringBlock))));
                Log.e("debug","Xoa ID : "+StringBlock);
                SaveFile(listBlock_Time);
                _Block_TimeListViewAdapter.notifyDataSetChanged();

            }catch (Exception e)
            {
                Log.e("ERROR","ERROR Remove Note");
            }
        }
    }


        button_ADD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int list_Size = listBlock_Time.size();
                Intent intent_Add_time = new Intent(MainActivity.this,InputData.class);
                intent_Add_time.putExtra("Add_ID",list_Size);
                intent_Add_time.putExtra("Command","ADD");
                startActivity(intent_Add_time);
            }
        });
    }
    @NonNull
    private ArrayList<Block_Time> BigString2List(String BigString)
    {
        //String fullStr =    // String lớn

        String code_Head = "Block_Time{";
        ArrayList<Block_Time> listTemp;
        listTemp = new ArrayList<Block_Time>();
        while (true)
        {
            int loc_Head = BigString.indexOf(code_Head);
            if(loc_Head==-1)
            {
                break;
            }
            int loc_End_Head = BigString.indexOf('}', loc_Head);
            if(loc_End_Head==-1)
            {
                break;
            }
            String cut_String = BigString.substring(loc_Head, loc_End_Head+1);
            //Log.e("debug","Cut String : "+cut_String);
            listTemp.add(new Block_Time(cut_String));
            BigString = BigString.substring(loc_End_Head+1);
        }
        return listTemp;

    }
    private void SaveFile(ArrayList<Block_Time> listSave)
    {
        String SaveString = listSave.toString();
        boolean b_save = writeData(SaveString);
        if(b_save)
        {
            Log.e("debug","file saved successfully");
        }else {
            Log.e("debug", "file save failed");
        }

    }
    public String readData()
    {
        try {
            FileInputStream in= openFileInput("myfile.txt");
            BufferedReader reader=new BufferedReader(new InputStreamReader(in));
            String data="";
            StringBuilder builder=new StringBuilder();  // dữ liệu trong file sẽ được lưu bởi string
            while((data=reader.readLine())!=null)
            // data sẽ là các dòng
            {
                builder.append(data);
                builder.append("\n");
            }
            in.close();
            return builder.toString();

        } catch (FileNotFoundException e) {
            e.printStackTrace();// hiển thị lỗi trong debug
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     * Hàm ghi tập tin trong Android
     * dùng openFileOutput để ghi
     * openFileOutput tạo ra FileOutputStream
     */
    public boolean writeData(String dataSave)
    {
        Log.e("debug","Go to Write File");
        try {
            FileOutputStream out=
                    openFileOutput("myfile.txt",0);
            OutputStreamWriter writer=
                    new OutputStreamWriter(out);
            writer.write(dataSave);
            writer.close();
            return true;
        } catch (FileNotFoundException e) {

            e.printStackTrace();
            return false;
        } catch (IOException e) {

            e.printStackTrace();
            return false;
        }
    }


}