package com.example.myapplication;

import android.annotation.SuppressLint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class Block_Adapter extends BaseAdapter {

    //Dữ liệu liên kết bởi Adapter là một mảng các sản phẩm
    final ArrayList<Block_Time> listBlock_Time;

    public Block_Adapter(ArrayList<Block_Time> listBlock_Time) {
        this.listBlock_Time = listBlock_Time;
    }

    @Override
    public int getCount() {
        return listBlock_Time.size();
    }

    @Override
    public Object getItem(int position) {
        return listBlock_Time.get(position);
    }

    @Override
    public long getItemId(int position) {
        return listBlock_Time.get(position).ID_Block;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View viewBlock_Time;
        if (convertView == null) {
            viewBlock_Time = View.inflate(parent.getContext(), R.layout.designlist, null);
        } else viewBlock_Time = convertView;

        //Bind sữ liệu phần tử vào View
        Block_Time Block_Time = (Block_Time) getItem(position);

        ((TextView) viewBlock_Time.findViewById(R.id.textView)).setText("MÔN: "+Block_Time.get_name_course());
        ((TextView) viewBlock_Time.findViewById(R.id.location)).setText("Địa Điểm: "+Block_Time.getLocation());
        //Block_Time.CoverTime();
        ((TextView) viewBlock_Time.findViewById(R.id.textView6)).setText("Time: "+Block_Time.getTime());

        if(Block_Time.get_day_boolean(0))
        {       viewBlock_Time.findViewById(R.id.textView4).setVisibility(View.VISIBLE);    }
        else{   viewBlock_Time.findViewById(R.id.textView4).setVisibility(View.INVISIBLE);    }
        if(Block_Time.get_day_boolean(1))
        {       viewBlock_Time.findViewById(R.id.textView3).setVisibility(View.VISIBLE);    }
        else{   viewBlock_Time.findViewById(R.id.textView3).setVisibility(View.INVISIBLE);    }
        if(Block_Time.get_day_boolean(2))
        {       viewBlock_Time.findViewById(R.id.textView2).setVisibility(View.VISIBLE);    }
        else{   viewBlock_Time.findViewById(R.id.textView2).setVisibility(View.INVISIBLE);    }
        if(Block_Time.get_day_boolean(3))
        {       viewBlock_Time.findViewById(R.id.textView5).setVisibility(View.VISIBLE);    }
        else{   viewBlock_Time.findViewById(R.id.textView5).setVisibility(View.INVISIBLE);    }
        if(Block_Time.get_day_boolean(4))
        {       viewBlock_Time.findViewById(R.id.textView9).setVisibility(View.VISIBLE);    }
        else{   viewBlock_Time.findViewById(R.id.textView9).setVisibility(View.INVISIBLE);    }
        if(Block_Time.get_day_boolean(5))
        {       viewBlock_Time.findViewById(R.id.textView7).setVisibility(View.VISIBLE);    }
        else{   viewBlock_Time.findViewById(R.id.textView7).setVisibility(View.INVISIBLE);    }
        if(Block_Time.get_day_boolean(6))
        {       viewBlock_Time.findViewById(R.id.textView10).setVisibility(View.VISIBLE);    }
        else{   viewBlock_Time.findViewById(R.id.textView10).setVisibility(View.INVISIBLE);    }


        return viewBlock_Time;
    }
}
