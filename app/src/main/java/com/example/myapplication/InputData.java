package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import java.util.ArrayList;

public class InputData extends AppCompatActivity {
    CheckBox check_Day_2;
    CheckBox check_Day_3;
    CheckBox check_Day_4;
    CheckBox check_Day_5;
    CheckBox check_Day_6;
    CheckBox check_Day_7;
    CheckBox check_Day_CN;
    //======================
    CheckBox checkLession_1;
    CheckBox checkLession_2;
    CheckBox checkLession_3;
    CheckBox checkLession_4;
    CheckBox checkLession_5;
    CheckBox checkLession_6;
    CheckBox checkLession_7;
    CheckBox checkLession_8;
    CheckBox checkLession_9;
    CheckBox checkLession_10;
    CheckBox checkLession_11;
    CheckBox checkLession_12;
    CheckBox checkLession_13;
    CheckBox checkLession_14;
    CheckBox checkLession_15;
    CheckBox checkLession_16;
    Button btn_del;
    Button btn_save;
    Button btn_out;
    EditText ET_course;
    EditText ET_gvhd;
    EditText ET_location;
    String S_Course;
    String S_GVHD;
    String S_location;
    Block_Time blockl_main;
    //ArrayList<CheckBox> List_CheckBox_Lesion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_data);
        //List_CheckBox_Lesion = new ArrayList<CheckBox>();
        findID_Day();
        findID_lession();
        btn_del = (Button) findViewById(R.id.btn_Del);
        btn_save = (Button) findViewById(R.id.btn_Save);
        btn_out = (Button) findViewById(R.id.btn_out);
        ET_course = findViewById(R.id.edit_course);
        ET_gvhd = findViewById(R.id.id_gvhd);
        ET_location = findViewById(R.id.id_location);

        Intent integer = getIntent();
        int Add_ID = integer.getIntExtra("Add_ID",0);
        String command = integer.getStringExtra("Command");
        if(command.equals("ADD"))
        {
            Log.e("debug","Goto if : "+Add_ID);
            blockl_main = new Block_Time();
            blockl_main.setID_Block(Add_ID);
        }else if(command.equals("GET"))
        {
            String data_get = integer.getStringExtra("Block");
            blockl_main = new Block_Time(data_get);
            show_Time();
        }
        else
        {
            blockl_main = new Block_Time();
        }
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                read_Day();
                read_Lession();
                blockl_main.CoverTime();
                // tính toán xem với các tiết học thì cần học từ mấy giờ đến mấy giờ
                try {
                    S_Course = ET_course.getText().toString();
                    S_GVHD = ET_gvhd.getText().toString();
                    S_location = ET_location.getText().toString();
                    blockl_main.set_name_course(S_Course);
                    blockl_main.set_GVHD(S_GVHD);
                    blockl_main.setLocation(S_location);
                    Log.e("debug","block_main"+blockl_main.toString());
                    Intent integerHome = new Intent(InputData.this,MainActivity.class);
                    integerHome.putExtra("command","SAVE");
                    integerHome.putExtra("StringBlock",blockl_main.toString());
                    startActivity(integerHome);
                }catch (Exception e )
                {
                    Log.e("ERROR","Error in getText from EditText");
                }

            }
        });
        btn_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent integerHome = new Intent(InputData.this,MainActivity.class);
                integerHome.putExtra("command","DEL");
                integerHome.putExtra("StringBlock",String.valueOf(Add_ID));
                startActivity(integerHome);
            }
        });
        btn_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent integerHome = new Intent(InputData.this,MainActivity.class);
                startActivity(integerHome);

            }
        });
    }

    private void read_Day()
    {
        //"if(check_Day_"+(i+2)+".isChecked()){blockl_main.set_Day(true,"+i+");}else{blockl_main.set_Day(false,"+i+");}";
        if(check_Day_2.isChecked()) {   blockl_main.set_Day(true,0);    }else{  blockl_main.set_Day(false,0);   }
        if(check_Day_3.isChecked()) {   blockl_main.set_Day(true,1);    }else{  blockl_main.set_Day(false,1);   }
        if(check_Day_4.isChecked()) {   blockl_main.set_Day(true,2);    }else{  blockl_main.set_Day(false,2);   }
        if(check_Day_5.isChecked()) {   blockl_main.set_Day(true,3);    }else{  blockl_main.set_Day(false,3);   }
        if(check_Day_6.isChecked()) {   blockl_main.set_Day(true,4);    }else{  blockl_main.set_Day(false,4);   }
        if(check_Day_7.isChecked()) {   blockl_main.set_Day(true,5);    }else{  blockl_main.set_Day(false,5);   }
        if(check_Day_7.isChecked()) {   blockl_main.set_Day(true,5);    }else{  blockl_main.set_Day(false,5);   }
        if(check_Day_CN.isChecked()) {   blockl_main.set_Day(true,6);    }else{  blockl_main.set_Day(false,6);   }

    }
    private void read_Lession()
    {
        /*
        for(int i = 0;i<16;i++)
        {
            String Data  = "if(checkLession_1"+(i+1)+".isChecked()) {   blockl_main.set_Lesson(true,"+i+");    }else{  blockl_main.set_Lesson(false,"+i+");   }";
        }

         */
        if(checkLession_1.isChecked()) {   blockl_main.set_Lesson(true,0);    }else{  blockl_main.set_Lesson(false,0);   }
        if(checkLession_2.isChecked()) {   blockl_main.set_Lesson(true,1);    }else{  blockl_main.set_Lesson(false,1);   }
        if(checkLession_3.isChecked()) {   blockl_main.set_Lesson(true,2);    }else{  blockl_main.set_Lesson(false,2);   }
        if(checkLession_4.isChecked()) {   blockl_main.set_Lesson(true,3);    }else{  blockl_main.set_Lesson(false,3);   }
        if(checkLession_5.isChecked()) {   blockl_main.set_Lesson(true,4);    }else{  blockl_main.set_Lesson(false,4);   }
        if(checkLession_6.isChecked()) {   blockl_main.set_Lesson(true,5);    }else{  blockl_main.set_Lesson(false,5);   }
        if(checkLession_7.isChecked()) {   blockl_main.set_Lesson(true,6);    }else{  blockl_main.set_Lesson(false,6);   }
        if(checkLession_8.isChecked()) {   blockl_main.set_Lesson(true,7);    }else{  blockl_main.set_Lesson(false,7);   }
        if(checkLession_9.isChecked()) {   blockl_main.set_Lesson(true,8);    }else{  blockl_main.set_Lesson(false,8);   }
        if(checkLession_10.isChecked()) {   blockl_main.set_Lesson(true,9);    }else{  blockl_main.set_Lesson(false,9);   }
        if(checkLession_11.isChecked()) {   blockl_main.set_Lesson(true,10);    }else{  blockl_main.set_Lesson(false,10);   }
        if(checkLession_12.isChecked()) {   blockl_main.set_Lesson(true,11);    }else{  blockl_main.set_Lesson(false,11);   }
        if(checkLession_13.isChecked()) {   blockl_main.set_Lesson(true,12);    }else{  blockl_main.set_Lesson(false,12);   }
        if(checkLession_14.isChecked()) {   blockl_main.set_Lesson(true,13);    }else{  blockl_main.set_Lesson(false,13);   }
        if(checkLession_15.isChecked()) {   blockl_main.set_Lesson(true,14);    }else{  blockl_main.set_Lesson(false,14);   }
        if(checkLession_16.isChecked()) {   blockl_main.set_Lesson(true,15);    }else{  blockl_main.set_Lesson(false,15);   }
    }
    private void findID_Day()
    {
        check_Day_2 = (CheckBox) findViewById(R.id.checkbox_Day_2);
        check_Day_3 = (CheckBox) findViewById(R.id.checkbox_Day_3);
        check_Day_4 = (CheckBox) findViewById(R.id.checkbox_Day_4);
        check_Day_5 = (CheckBox) findViewById(R.id.checkbox_Day_5);
        check_Day_6 = (CheckBox) findViewById(R.id.checkbox_Day_6);
        check_Day_7 = (CheckBox) findViewById(R.id.checkbox_Day_7);
        check_Day_CN = (CheckBox) findViewById(R.id.checkbox_Day_cn);
    }
    private void findID_lession()
    {
        checkLession_1 = (CheckBox) findViewById(R.id.tiet_1);//  List_CheckBox_Lesion.add(checkLession_1);
        checkLession_2 = (CheckBox) findViewById(R.id.tiet_2);//  List_CheckBox_Lesion.add(checkLession_2);
        checkLession_3 = (CheckBox) findViewById(R.id.tiet_3);//  List_CheckBox_Lesion.add(checkLession_3);
        checkLession_4 = (CheckBox) findViewById(R.id.tiet_4);//  List_CheckBox_Lesion.add(checkLession_4);
        checkLession_5 = (CheckBox) findViewById(R.id.tiet_5);//  List_CheckBox_Lesion.add(checkLession_5);
        checkLession_6 = (CheckBox) findViewById(R.id.tiet_6);//  List_CheckBox_Lesion.add(checkLession_6);
        checkLession_7 = (CheckBox) findViewById(R.id.tiet_7);//  List_CheckBox_Lesion.add(checkLession_7);
        checkLession_8 = (CheckBox) findViewById(R.id.tiet_8);//  List_CheckBox_Lesion.add(checkLession_8);
        checkLession_9 = (CheckBox) findViewById(R.id.tiet_9);//  List_CheckBox_Lesion.add(checkLession_9);
        checkLession_10 = (CheckBox) findViewById(R.id.tiet_10);//  List_CheckBox_Lesion.add(checkLession_10);
        checkLession_11 = (CheckBox) findViewById(R.id.tiet_11);//  List_CheckBox_Lesion.add(checkLession_11);
        checkLession_12 = (CheckBox) findViewById(R.id.tiet_12);///  List_CheckBox_Lesion.add(checkLession_12);
        checkLession_13 = (CheckBox) findViewById(R.id.tiet_13);///  List_CheckBox_Lesion.add(checkLession_13);
        checkLession_14 = (CheckBox) findViewById(R.id.tiet_14);//  List_CheckBox_Lesion.add(checkLession_14);
        checkLession_15 = (CheckBox) findViewById(R.id.tiet_15);//  List_CheckBox_Lesion.add(checkLession_15);
        checkLession_16 = (CheckBox) findViewById(R.id.tiet_16);//  List_CheckBox_Lesion.add(checkLession_16);
    }
    private void show_Time()
    {

         ET_course.setText(blockl_main._name_course);
         ET_gvhd.setText(blockl_main._GVHD);
         ET_location.setText(blockl_main.location);
        check_Day_2.setChecked(blockl_main.get_day_boolean(0));
        check_Day_3.setChecked(blockl_main.get_day_boolean(1));
        check_Day_4.setChecked(blockl_main.get_day_boolean(2));
        check_Day_5.setChecked(blockl_main.get_day_boolean(3));
        check_Day_6.setChecked(blockl_main.get_day_boolean(4));
        check_Day_7.setChecked(blockl_main.get_day_boolean(5));
        check_Day_CN.setChecked(blockl_main.get_day_boolean(6));

        checkLession_1.setChecked(blockl_main.get_lesson_boolean(0));
        checkLession_2.setChecked(blockl_main.get_lesson_boolean(1));
        checkLession_3.setChecked(blockl_main.get_lesson_boolean(2));
        checkLession_4.setChecked(blockl_main.get_lesson_boolean(3));
        checkLession_5.setChecked(blockl_main.get_lesson_boolean(4));
        checkLession_6.setChecked(blockl_main.get_lesson_boolean(5));
        checkLession_7.setChecked(blockl_main.get_lesson_boolean(6));
        checkLession_8.setChecked(blockl_main.get_lesson_boolean(7));
        checkLession_9.setChecked(blockl_main.get_lesson_boolean(8));
        checkLession_10.setChecked(blockl_main.get_lesson_boolean(9));
        checkLession_11.setChecked(blockl_main.get_lesson_boolean(10));
        checkLession_12.setChecked(blockl_main.get_lesson_boolean(11));
        checkLession_13.setChecked(blockl_main.get_lesson_boolean(12));
        checkLession_14.setChecked(blockl_main.get_lesson_boolean(13));
        checkLession_15.setChecked(blockl_main.get_lesson_boolean(14));
        checkLession_16.setChecked(blockl_main.get_lesson_boolean(15));
    }

}