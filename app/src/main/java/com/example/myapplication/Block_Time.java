package com.example.myapplication;

import android.util.Log;

import androidx.annotation.NonNull;

import java.util.Arrays;

public class Block_Time {
    public int ID_Block;
    public String _name_course; // tên môn học
    String _GVHD;   // tên giáo viên
    //int num_Credit;    // số tín
    String location;    // địa điểm "A1 1503"
    int _lesson;       // tiết lý thuyết    16 bit
    int _practice;          // tiết thực hành
    int _day;              // ngày
    String data_Send_time = " ";

    public Block_Time(String _name_course, String _GVHD, int num_Credit, String location) {
        this._name_course = _name_course;
        this._GVHD = _GVHD;
        this.location = location;
        this._lesson = 0;
        this._practice = 0;
        this._day=0;
    }
    public Block_Time() {
        this._name_course = null;
        this._GVHD = null;
        this.location = null;
        this._lesson = 0;
        this._practice = 0;
        this._day=0;
    }
    public Block_Time(String data)
    {
        String2Block(data);
    }
    public void set_name_course(String _name_course) {
        this._name_course = _name_course;
    }

    public void set_GVHD(String _GVHD) {
        this._GVHD = _GVHD;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setID_Block(int ID_Block) {
        this.ID_Block = ID_Block;
    }

    public void set_Lesson(boolean data, int num) {     // tiết ly thuyết
        if (num > 16 || num < 0)
            System.out.print("Out Lesson Class BlockCourse");
        else
        {
            if(data)
            {
                int temp = 0b0000000000000001;
                temp <<= num;
                int tempLes = this._lesson;
                tempLes = tempLes | temp;
                this._lesson = tempLes;
            }else {
                int temp = 0b0000000000000001;
                temp <<= num;
                int tempLes = this._lesson;
                tempLes = tempLes & ~temp;
                this._lesson = tempLes;
            }
        }
    }

    public void set_Practice(boolean data, int num) {
        if (num > 2 || num < 0)
            Log.e("ERROR","So truyen vao parctice vuot ngoai pham vi");
        else {
            if(data)
            {
                int temp = 0b0000000000000001;
                temp <<= num;
                int tempLes = this._practice;
                tempLes = tempLes | temp;
                this._practice = tempLes;
            }else {
                int temp = 0b0000000000000001;
                temp <<= num;
                int tempLes = this._practice;
                tempLes = tempLes & ~temp;
                this._practice = tempLes;
            }
        }
    }

    public void set_Day(boolean data, int num) {
        if (num > 7 || num < 0)
            Log.e("ERROR","So truyen vao ngay vuot ngoai pham vi");
        else
        {
            if(data)
            {
                int temp = 0b0000000000000001;
                temp <<= num;
                int tempLes = this._day;
                tempLes = tempLes | temp;   // set bit số num của tempLet lên 1
                this._day = tempLes;
            }else {
                int temp = 0b0000000000000001;
                temp <<= num;
                int tempLes = this._day;
                tempLes = tempLes & ~temp;// set bit số num của tempLet Về 0
                this._day = tempLes;
            }
        }
    }

    public int get_day_int() {
        return this._day;
    }
    public boolean get_day_boolean(int num) {
        int temp = 0b0000000000000001;
        temp <<= num;
        int temp2 = this._day;
        if((temp2 & temp)>0)
            return true;
        else
            return false;
    }

    public int get_lesson_int() {
        return this._lesson;
    }
    public boolean get_lesson_boolean(int num) {
        int temp = 0b0000000000000001;
        temp <<= num;
        int temp2 = this._lesson;
        Log.e("debug","get_lesson_boolean");
        if((temp2 & temp)>0)
        {
            Log.e("debug","_lesson_boolean("+num+") = true");
            return true;
        }
        else
        {
            Log.e("debug","_lesson_boolean("+num+") = false");
            return false;
        }

    }
    public int get_Practice_int() {
        return this._practice;
    }
    public boolean get_Practice_boolean(int num) {
        int temp = 0b0000000000000001;
        temp <<= num;
        int temp2 = this._practice;
        if((temp2 & temp)>0)
            return true;
        else
            return false;
    }
    public int get_ID() {
        return ID_Block;
    }

    public String get_name_course() {
        return _name_course;
    }
    public String getLocation() {
        return location;
    }
    public String get_GVHD() {
        return _GVHD;
    }

    public void CoverTime() {
        // thời gian tính bằng số phút của tiết học trong ngày
        int end_time = 0, start_time = 1440;     // giờ bắt đầu học và giờ kết thúc
        for (int couter_crus = 0; couter_crus < 16; couter_crus++) {
            if (get_lesson_boolean(couter_crus)) {

                int temp;
                switch (couter_crus) {
                    case 0:
                        temp = 7 * 60;  // giờ bắt đàu * 60 phút
                        if (end_time < (temp + 50)) end_time = (temp + 50); // nếu có 1 thời gian kết thúc muộn hơn thời gian tiết học này
                        if (start_time > temp) start_time = temp;
                        break;
                    case 1:
                        temp = 7 * 60 + 50;
                        if (end_time < (temp + 50)) end_time = (temp + 50);
                        if (start_time > temp) start_time = temp;
                        break;
                    case 2:
                        temp = 8 * 60 + 50;
                        if (end_time < (temp + 50)) end_time = (temp + 50);
                        if (start_time > temp) start_time = temp;
                        break;
                    case 3:
                        temp = 9 * 60 + 50;
                        if (end_time < (temp + 50)) end_time = (temp + 50);
                        if (start_time > temp) start_time = temp;
                        break;
                    case 4:
                        temp = 10 * 60 + 40;
                        if (end_time < (temp + 50)) end_time = (temp + 50);
                        if (start_time > temp) start_time = temp;
                        break;
                    case 5:
                        temp = 11 * 60 + 30;
                        if (end_time < (temp + 60)) end_time = (temp + 60);
                        if (start_time > temp) start_time = temp;
                        break;
                    case 6:
                        temp = 12 * 60 + 30;
                        if (end_time < (temp + 50)) end_time = (temp + 50);
                        if (start_time > temp) start_time = temp;
                        break;
                    case 7:
                        temp = 13 * 60 + 20;
                        if (end_time < (temp + 50)) end_time = (temp + 50);
                        if (start_time > temp) start_time = temp;
                        break;
                    case 8:
                        temp = 14 * 60 + 20;
                        if (end_time < (temp + 50)) end_time = (temp + 50);
                        if (start_time > temp) start_time = temp;
                        break;
                    case 9:
                        temp = 15 * 60 + 20;
                        if (end_time < (temp + 50)) end_time = (temp + 50);
                        if (start_time > temp) start_time = temp;
                        break;
                    case 10:
                        temp = 16 * 60 + 10;
                        if (end_time < (temp + 50)) end_time = (temp + 50);
                        if (start_time > temp) start_time = temp;
                        break;
                    case 11:
                        temp = 17 * 60;
                        if (end_time < (temp + 30)) end_time = (temp + 30);
                        if (start_time > temp) start_time = temp;
                        break;
                    case 12:
                        temp = 17 * 60 + 30;
                        if (end_time < (temp + 50)) end_time = (temp + 50);
                        if (start_time > temp) start_time = temp;
                        break;
                    case 13:
                        temp = 18 * 60 + 20;
                        if (end_time < (temp + 50)) end_time = (temp + 50);
                        if (start_time > temp) start_time = temp;
                        break;
                    case 14:
                        temp = 19 * 60 + 20;
                        if (end_time < (temp + 50)) end_time = (temp + 50);
                        if (start_time > temp) start_time = temp;
                        break;
                    case 15:
                        temp = 20 * 60 + 10;
                        if (end_time < (temp + 50)) end_time = (temp + 50);
                        if (start_time > temp) start_time = temp;
                        break;
                    default:
                        break;
                }
            }

        }

        data_Send_time = "Start: ";
        data_Send_time += String.valueOf(start_time / 60);
        data_Send_time += "h: ";
        data_Send_time += String.valueOf(start_time - (start_time / 60) * 60);
        data_Send_time += "m|";
        data_Send_time += "End : ";
        data_Send_time += String.valueOf(end_time / 60);
        data_Send_time += "h: ";
        data_Send_time += String.valueOf(end_time - (end_time / 60) * 60);
        data_Send_time += "m";
        Log.e("debug","data_Send_time = "+data_Send_time);


    }
    public String getTime() {
        return data_Send_time;
    }

    @Override
    public String toString() {
        String out = "Block_Time{" +
                "ID_Block=" + ID_Block +
                ", _name_course='" + _name_course + '\'' +
                ", _GVHD='" + _GVHD + '\'' +
                ", location='" + location + '\'' +
                ", _lesson=" + _lesson +
                ", _practice=" + _practice +
                ", _day=" + _day +
                ", data_Send_time='" + data_Send_time + '\'' +
                '}';
        return out.concat("\n");
    }

    public void String2Block(String data)
    {
        String code_ID = "ID_Block=";
        int loaction_ID = data.indexOf("ID_Block=")+code_ID.length();
        int loaction_End_ID = data.indexOf(",", loaction_ID);
        this.ID_Block = Integer.parseInt(data.substring(loaction_ID, loaction_End_ID));
        String code_Head = "_name_course='";
        int loaction_course = data.indexOf(code_Head)+code_Head.length();
        int loaction_End_course = data.indexOf('\'', loaction_course);
        this._name_course = data.substring(loaction_course, loaction_End_course);

        String code_Time = "data_Send_time='";
        int loaction_Time = data.indexOf(code_Time)+code_Time.length();
        int loaction_End_Time = data.indexOf('\'', loaction_Time);
        this.data_Send_time = data.substring(loaction_Time, loaction_End_Time);

        String code_Content = "_GVHD='";
        int loaction_GVHD = data.indexOf(code_Content)+code_Content.length();
        int loaction_End_GVHD = data.indexOf('\'', loaction_GVHD);
        this._GVHD = data.substring(loaction_GVHD, loaction_End_GVHD);

        String code_Noted = "location='";
        int loaction_location = data.indexOf(code_Noted)+code_Noted.length();
        int loaction_End_location = data.indexOf('\'', loaction_location);
        this.location = data.substring(loaction_location, loaction_End_location);

        String code_lession = "_lesson=";
        int loaction_lesson = data.indexOf(code_lession)+code_lession.length();
        int loaction_End_lesson = data.indexOf(",", loaction_lesson);
        Log.e("debug","_lesson = "+data.substring(loaction_lesson, loaction_End_lesson));

        this._lesson = Integer.parseInt(data.substring(loaction_lesson, loaction_End_lesson));

        String code_practice = "_practice=";
        int loaction_practice = data.indexOf("_practice=")+code_practice.length();
        int loaction_End_practice = data.indexOf(",", loaction_practice);
        this._practice = Integer.parseInt(data.substring(loaction_practice, loaction_End_practice));

        String code_day = "_day=";
        int loaction_day = data.indexOf("_day=")+code_day.length();
        int loaction_End_day = data.indexOf(",", loaction_day);
        this._day = Integer.parseInt(data.substring(loaction_day, loaction_End_day));

    }
}
